<?php

class Books_model extends Model {

    public function get_books(){
        $result = $this->db('SELECT b.id,b.title,b.language,b.publication_date,b.isbn_num,b.image,g.genre_name FROM books b LEFT JOIN genre g on(b.genre_id=g.id)');
        $books = [];
        while($row = $result->fetch_assoc()) {
            $books[] = $row;
        }
        return $books;
    }

    public function add_book($book){

        $keys = implode(",", array_keys($book));
        $values = implode("','", array_values($book));
        return $this->db("INSERT INTO `books` (".$keys.") VALUES ('".$values."')");
    }

    public function delete_book($book_id){
        $this->db('DELETE FROM books WHERE id = '.$book_id);
    }
}