
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog</title>

    <!-- Bootstrap Core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/public/css/blog-home.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="padding-top: 70px">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Start Bootstrap</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/home/register'; ?>">Sign in</a>
                </li>
                <li>
                    <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/home/login'; ?>">Login</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-12">
            <h2 class="form-signin-heading">Books list</h2>
            <a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/books/add'; ?>" class="btn btn-primary">Add new</a>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Genre</th>
                    <th>Language</th>
                    <th>Date</th>
                    <th>ISBN</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($data['books'] as $key => $value): ?>
                <tr>
                    <th scope="row"><?= $key+1; ?></th>
                    <td><?= $value['title']; ?></td>
                    <th></th>
                    <th><?= $value['genre_id']; ?></th>
                    <th><?= $value['language']; ?></th>
                    <th><?= $value['publication_date']; ?></th>
                    <th><?= $value['isbn_num']; ?></th>
                    <th><a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/books/edit?id='.$value['id']; ?>">Edit</a></th>
                    <th><a href="<?= 'http://'.$_SERVER['SERVER_NAME'].'/books/delete?id='.$value['id']; ?>">Delete</a></th>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <footer style="margin: 50px 0">
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Your Website <?= date('Y', time()); ?></p>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>