<?php
class Model{
    protected function db($sql){
        //database credentials
        $servername = "localhost";
        $username = "blog";
        $password = "blog";
        $dbname = "blog";
        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if (mysqli_connect_error()) {
            die("Database connection failed: " . mysqli_connect_error());
        }

        $query = $conn->query($sql);
        $conn->close();

        return $query;
    }
}