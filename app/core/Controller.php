<?php

class Controller{
    protected function model($model){
        require_once '../app/models/'.ucfirst($model).'.php';
        return new $model();
    }

    protected function view($view, $data = []){
        require_once '../app/view/'.$view.'.php';
    }

    protected function check_session(){
        session_start();
        if (!isset($_SESSION['username'])) {
            header('Location:'.$_SERVER['SERVER_NAME'].'/home');
        }
    }
    
    protected function upload_image($image){
        if(isset($image['tmp_name'])){
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check['mime'] == 'image/jpeg' || $check['mime'] == 'image/png') {
                $extension = explode('.', $_FILES["image"]['name']);
                $name = md5($image['name'].rand(100,999)) . '.' . $extension[1];
                if (move_uploaded_file($image['tmp_name'], '../public/assets/img/' . $name)) {
                    return $name;
                }
                return false;
            }
        }
    }
}