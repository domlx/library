<?php
class Home extends Controller{

    public function Index(){
        session_start();
        if (isset($_SESSION['username'])) {
            $this->view('home');
        }else{
            header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/home/login');
        }
    }

    public function posts_api(){
        $user = $this->model('user_model');
        $data =$user->posts();

        header('Content-Type: application/json', true, 200);
        echo json_encode($data);
    }

    public function register(){
        $this->view('register');
    }

    public function add(){
        $user = $this->model('user_model');
        if (isset($_POST['username']) && isset($_POST['password'])){
            $result = $user->add_user($_POST['username'], $_POST['password']);

            if($result){
                $data['smsg'] = "User Created Successfully.";
            }else{
                $data['fmsg'] ="User Registration Failed";
            }

            $this->view('register', $data);
        }else{
            $this->view('register');
        }

    }


    public function login(){
        $this->view('login');
    }


    public function login_check(){
        $user = $this->model('user_model');
        if (isset($_POST['username']) && isset($_POST['password'])){
            $result = $user->check_login($_POST['username'], $_POST['password']);

            if ($result == 1){
                session_start();
                $_SESSION['username'] = $_POST['username'];
                header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/home');
            }else{
                $data['fmsg'] = "Invalid Login Credentials.";
                $this->view('login', $data);
            }
        }else{
            $this->view('login');
        }

    }

    public function logout(){
        session_start();
        session_destroy();
        header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/home');
    }

}