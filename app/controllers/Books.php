<?php
class Books extends Controller{

    public function __construct(){
        $this->check_session();
    }

    public function index(){
        $data['books']= $this->model('Books_model')->get_books();
        $this->view('books', $data);
    }

    public function add(){
        $this->view('add_book');
    }

    public function insert(){
        $title=filter_input(INPUT_POST,"title",FILTER_SANITIZE_STRING);
        $genre_id=filter_input(INPUT_POST,"genre_id",FILTER_SANITIZE_EMAIL);
        $language=filter_input(INPUT_POST,"language",FILTER_SANITIZE_STRING);
        $publication_date=filter_input(INPUT_POST,"publication_date",FILTER_SANITIZE_STRING);
        $isbn_number=filter_input(INPUT_POST,"isbn_num",FILTER_SANITIZE_STRING);
        $book = [
            'title' => $title,
            'genre_id' => $genre_id,
            'language' => $language,
            'publication_date' => date('Y-m-d', strtotime($publication_date)),
            'isbn_num' => $isbn_number
        ];

        if(isset($_FILES['image']['tmp_name'])){
            $file_name = $this->upload_image($_FILES['image']);
            if($file_name){
                $book['image'] = $file_name;
            }
        }
        $this->model('Books_model')->add_book($book);
        header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/books');
    }


    public function edit(){
        //I'll explain in interview how to do this
    }

    public function delete(){
        $this->model('Books_model')->delete_book($_GET['id']);
        header('Location: '.'http://'.$_SERVER['SERVER_NAME'].'/books');
    }
}